'''
parameter_parser.py -- parser for extended pcs format

@author:     Marius Lindauer
        
@copyright:  2014 Potassco. All rights reserved.
        
@license:    GPL

@contact:    manju@cs.uni-potsdam.de
'''

import traceback
from sets import Set
import sys

class ParameterParser(object):
    ''' parse special parameter syntax to executable syntax '''
    
    
    def __init__(self):
        ''' constructor '''
        pass
    
    def parse_parameters(self,params,prefix="--",separator="="):
        ''' parse parameters; syntax: -@[Thread]:{[component]:|S|F}*[param] [value]#
            globals parameters at thread-id 0
            Args:
                parameter : parameter list
                prefix: of build parameters, e.g. "--" (can be overwritten by parameter file)
                separator: of build parameters, e.g. "=" for --t=8 or " " for --t 8
            Returns:
                thread_to_params:    thread-id to list of parameters (0 -> global parameters)
                thread_to_solver:    thread-id to solver binary
                params_to_tags:      parameter name to smac like tags 
        '''     
        params_to_tags = {}
        thread_to_params = {}
        thread_to_solver = {}
        parallel_thread_list = []
        threadIndex = 0
        while params:
            localParam = {}
            local_prefix = prefix
            local_separator = separator
            value = ""
            option = ""
            backupparams = []
            while(params != []):
                head = params.pop(0)
                value = params.pop(0).replace("'","")
                value_is_int = False
                try:
                    value = int(value)
                    value = str(value)
                    value_is_int = True
                except ValueError:
                    value_is_int = False
                if not value_is_int:
                    try:
                        value = round(float(value),4) # some solvers have problems with E notation, e.g. 6,37E8
                        value = str(value)
                    except ValueError:
                        pass
                if (head.startswith("-@"+str(threadIndex))):
                    option = head[4:]
                    optionSplits = option.split(":")
                    parameterName = optionSplits.pop() # last item in list = parameterName
                    if (parameterName == "solver"): # special filter for solver binary path
                        thread_to_solver[threadIndex] = value
                        continue
                    if (parameterName == "prefix"):
                        local_prefix = value
                        continue
                    if (parameterName == "separator"):
                        if value == "W": # parameter with only whitespaces is not allowed by SMAC
                            value = " "
                        local_separator = value
                        continue
                    if len(optionSplits) > 0:  # composed multi parameter
                        skip = False
                        flag = False
                        component = -1
                        if (params_to_tags.get(parameterName) == None):
                            params_to_tags[parameterName] = Set()
                        for opt in optionSplits:
                            if (opt == "F"): # Flag
                                flag = True
                                continue
                            if (opt == "S"): # skip
                                skip = True
                                continue
                            try:
                                component = int(opt)
                            except ValueError:
                                params_to_tags[parameterName].add(opt)
                        if (skip == True):
                            continue
                        if (flag == True and value == "no"):
                            continue # won't be passed to solver
                        if (flag == True and value == "yes"):
                            value = ""
                        if (component != -1):
                            if (localParam.get(parameterName) == None):
                                localParam[parameterName] = {}
                            localParam[parameterName][component] = value
                        else:
                            localParam[parameterName] = value
                        tags = params_to_tags[parameterName]
                        if "ITERATIVE" in tags:
                            parallel_thread_list.append(threadIndex)
                    else:
                        localParam[option] = value
                else:
                    backupparams.append(head) # keep unhandled
                    backupparams.append(value)
            params = backupparams
            if not (len(localParam) == 0 and threadIndex > 0):
                thread_to_params[threadIndex] = self.__dic_to_flat_list(localParam, local_prefix, local_separator)
            threadIndex += 1
        #print(thread_to_params)
        #print(" ".join(thread_to_params[1]))
        new_params_t1 = self.__join_tags(thread_to_params, params_to_tags, "feature-steps")
        #print(" ".join(new_params_t1))
        thread_to_params[1] = new_params_t1
        new_params_t1 = self.__join_tags(thread_to_params, params_to_tags, "features")
        #print(" ".join(new_params_t1))
        thread_to_params[1] = new_params_t1
        new_params_t1 = self.__join_tags(thread_to_params, params_to_tags, "algorithms")
        #print(" ".join(new_params_t1))
        thread_to_params[1] = new_params_t1
        return thread_to_params, thread_to_solver, params_to_tags
    
    def __join_tags(self, thread_to_params, params_to_tags, tag):
        #print(">>>> %s" %(tag))
        params = list(thread_to_params[1])
        active_params = []
        new_params = [] 
        while params:
            p = params.pop(0)
            p_stripped = p.lstrip("-")
            #print(p_stripped)
            if params_to_tags.get(p_stripped) and tag in params_to_tags[p_stripped]:
                active_params.append(p_stripped)
                #print(p_stripped)
            else:
                new_params.append(p)
        if active_params:
            new_params.append("--%s" %(tag))
            for p in active_params:
                new_params.append(p)
        return new_params
        
    def __sorted_dict_values(self,adict):
        keys = adict.keys()
        keys.sort()
        return map(adict.get, keys)
    
    def __dic_to_flat_list(self, adict, prefix, separator):
        '''
            convert dictionary to flat parameter list
            Args:
                adict: dictionary : head -> value
                prefix: prefix of parameter head, e.g. "--"
        '''
        allP = []
        for k,v in adict.items():
            if (type(v) is dict):
                vsort = self.__sorted_dict_values(v)
                sortedvalues = ",".join(vsort)
                if separator == " ":
                    allP.append(prefix+k)
                    allP.append(sortedvalues)
                else:
                    allP.append(prefix+k+separator+sortedvalues )
            else:
                if (type(v) is list):
                    allP.extend(v)
                else:
                    if (v == ""):   # flag handling
                        allP.append(prefix+k)
                    else:
                        if separator == " ":
                            allP.append(prefix+k)
                            allP.append(v)
                        else:
                            allP.append(prefix+k+separator+v)
                            
        return allP

