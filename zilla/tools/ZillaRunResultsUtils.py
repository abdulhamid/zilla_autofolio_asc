import sys
import os
import csv
import operator

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import mypyutils
import scipy.stats as stats

def numerizeRunResults(runresults):
    """
    Return the numeric version (i.e. runtimes, cutoff, seed, quality all numeric) of a run data dictionary
    created by another reader method (such as ZillaDataConverter.py).

    All the remaining methods deal with numeric run data dictionary.
    """
    rundata = dict()

    for instance in runresults.keys():
        for algorithm in runresults[instance].keys():

            (result,runtime,quality,cutoff,seed,additional_rundata) = runresults[instance][algorithm]

            if instance not in rundata.keys():
                rundata[instance] = dict()

            rundata[instance][algorithm] = (result,float(runtime),float(quality),float(cutoff),long(seed))

    return rundata


def addVBS(rundata):
    """
    Add Virtual Best Solver (VBS) data in the given run data. The VBS minimizes runtime across
    all the available algorithms for a given instance.

    rundata - the rundata dictionary.
    """
    vbs = 'VBS'

    for instance in rundata.keys():
        vbsruntime = None
        vbsquality = -1
        vbsresult = None
        for algorithm in rundata[instance].keys():
            (result,runtime,quality,cutoff,seed) = rundata[instance][algorithm]

            if vbsresult == None or ((result == 'SAT' or result == 'UNSAT') and (vbsruntime > runtime)):
                vbsruntime = runtime
                vbsresult = result

        if vbs in rundata[instance].keys():
            raise Exception('Algorithm VBS already in run data for instance '+instance+'.')

        rundata[instance][vbs] = (vbsresult,vbsruntime,vbsquality,cutoff,seed)



def getRunresults(rundata,algorithm,instances):
    """
    Return a dictionary from instance to run result for the given algorithm, across the given instances.

    rundata - the rundata dictionnary.
    algorithms - an algorithm to get run results for.
    instances - list of instances to evaluate.
    """
    runresults = {}

    for instance in instances:

        runresults[instance] = rundata[instance][algorithm]

    return runresults

def getNumberOfSolved(rundata,algorithms,instances):
    """
    Return an array of number of solved instances, where the i-th element of the array is the number
    of instances solved by the i-th algorithm in the given algorithms across all given instances.

    rundata - the rundata dictionnary.
    algorithms - list of algorithms to find mean PAR10 scores for.
    instances - list of instances to evaluate.
    """
    numberofsolved = []

    for algorithm in algorithms:
        solved = 0
        for instance in instances:
            (result,runtime,quality,cutoff,seed) = rundata[instance][algorithm]
            if result == 'SAT' or result =='UNSAT':
                solved+=1
        numberofsolved.append(solved)

    return numberofsolved

def getPercentOfSolved(rundata,algorithms,instances):
    number_solved = getNumberOfSolved(rundata,algorithms,instances)

    percent_solved = []

    for i in range(len(number_solved)):
        percent_solved.append(float(number_solved[i])/float(len(instances))*100.0)

    return percent_solved


def getMeanRuntimes(rundata,algorithms,instances):
    """
    Return an array of mean runtimes, where the i-th element of the array is the median
    runtime of the i-th algorithm in the given algorithms across all the given instances.

    rundata - the rundata dictionnary.
    algorithms - list of algorithms to find mean PAR10 scores for.
    instances - list of instances to evaluate.
    """
    meanruntimes = []
    for algorithm in algorithms:
        runtimes = []
        for instance in instances:
            (result,runtime,quality,cutoff,seed) = rundata[instance][algorithm]
            runtimes.append(runtime)
        meanruntimes.append(np.mean(runtimes))
    return meanruntimes



def getMedianRuntimes(rundata,algorithms,instances):
    """
    Return an array of median runtimes, where the i-th element of the array is the median
    runtime of the i-th algorithm in the given algorithms across all the given instances.

    rundata - the rundata dictionnary.
    algorithms - list of algorithms to find mean PAR10 scores for.
    instances - list of instances to evaluate.
    """
    medianruntimes = []

    for algorithm in algorithms:
        runtimes = []
        for instance in instances:
            (result,runtime,quality,cutoff,seed) = rundata[instance][algorithm]
            runtimes.append(runtime)
        medianruntimes.append(np.median(runtimes))

    return medianruntimes

def getMeanPARk(rundata,algorithms,instances,k):
    """
    Return an array of mean PAR10 scores for runtime, where the i-th element of the array
    is the mean PAR10 score of the i-th algorithm in the given algorithms across all given instances.

    rundata - the rundata dictionnary.
    algorithms - list of algorithms to find mean PAR10 scores for.
    instances - list of instances to evaluate.
    k - the PAR penalty.
    """
    meanPARk = []

    for algorithm in algorithms:
        PARks = []
        for instance in instances:
            (result,runtime,quality,cutoff,seed) = rundata[instance][algorithm]
            if runtime > cutoff or not(result == 'SAT' or result == 'UNSAT'):
                PARk = cutoff*k
            else:
                PARk = runtime
            PARks.append(PARk)
        meanPARk.append(np.mean(PARks))

    return meanPARk

