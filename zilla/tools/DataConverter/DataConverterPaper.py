import sys
import os

import csv

from ManualConverter import *
from SATzilla12Converter import *
from TAEEvaluationConverter import *
from ZillaConverter import *


#Zilla format specific string representing an unavailable/incomplete algorithm run (must coincide with Zilla code).
global ZF_INCOMPLETE_ALGORITHM_EXECUTION
ZF_INCOMPLETE_ALGORITHM_EXECUTION = "NA"
#Zilla format specific string representing an unavailable/incomplete feature value (must coincide with Zilla code).
global ZF_INCOMPLETE_FEATURE_VALUE
ZF_INCOMPLETE_FEATURE_VALUE = "-512"
#Zilla format specific string representing an unavailable/incomplete quality value (must coincide with Zilla code).
global ZF_INCOMPLETE_QUALITY_VALUE
ZF_INCOMPLETE_QUALITY_VALUE = "NaN"
#Zilla format specific string representing an unavailable seed.
global ZF_INCOMPLETE_SEED
ZF_INCOMPLETE_SEED = "0"

RUNRESULT_CONVERSIONS = {
                        'TAEeo2ZFrt':'TAE Evaluation Output to Zilla Format RunResult.',
                        'SATzilla12v12ZFrt':'SATzilla12 [instance, time, quality, solved] type run data to Zilla Format RunResult.',
                        'SATzilla12v22ZFrt':'SATzilla12 [instance, time, solved, quality] type run data to Zilla Format RunResult.',
                        'ZFrt2manual':'Zilla Format Run Results to manually processable format.',
                        'ZFrt2ZFrt':'Combine multiple Zilla Format RunResults, possibly merging them to a single algorithm (useful for cross validation settings).',
                        'manual2ZFrt':'Manually processable format to Zilla format run results.',
                        }
FEATURE_CONVERSIONS  = {
                        'TAEeo2ZFf':'TAE Evaluation Output to Zilla Format Features.',
                        'SATzilla12f2ZFf':'MIPzilla features to Zilla Format Features.'
                        }


CONVERSION_USAGE = '\n'.join(['\t'+conversion+' - '+RUNRESULT_CONVERSIONS[conversion] for conversion in RUNRESULT_CONVERSIONS.keys()])+'\n'+'\n'.join(['\t'+conversion+' - '+FEATURE_CONVERSIONS[conversion] for conversion in FEATURE_CONVERSIONS.keys()])
USAGE = """python ZillaDataConverter.py [OPTIONS] <conversion type> <input file1> <input file2> ... <input filek> <output file>
where
<conversion type> - type of conversion to perform. One of the following:
"""+CONVERSION_USAGE+"""
<input filei> - an input file to convert.
<output file> - the name of the file to write converted input to.
and
[OPTIONS] can be any of:
--overwrite-output <true/false> - whether to overwrite output that is already present.
--skip-valid <true/false> - whether skip data validation.
--remove-unsolved-instances <true/false> - whether to remove unsolved instances.
--one-algorithm <algorithm name> - merge all run results as if done by a single algorithm with given name.
--add-vbs <true/false> - whether to add the VBS to the data.
"""

def parseOption(args,option):
    if option in args:
        index = args.index(option)
        value = args[index+1]
        del args[index]
        del args[index]
        return value
    else:
        return None

def main():

    args = sys.argv

    #Parse options.
    overwrite_output = parseOption(args,'--overwrite-output')
    if overwrite_output != None:
        overwrite_output = bool(overwrite_output)
    skip_valid = bool(parseOption(args,'--skip-valid'))
    if skip_valid != None:
        skip_valid = bool(skip_valid)

    remove_unsolved_instances = ('true' == parseOption(args,'--remove-unsolved-instances'))
    print remove_unsolved_instances
    if remove_unsolved_instances != None:
        remove_unsolved_instances = bool(remove_unsolved_instances)

    one_algorithm = parseOption(args,'--one-algorithm')

    add_vbs = parseOption(args,'--add-vbs')

    for arg in args:
        if arg[:2] == '--':
            print '[ERROR] Unrecognized option %s.' % arg
            print 'Usage:'
            print USAGE
            return

    #Parse necessary arguments.

    if len(args) <=3:
        print '[ERROR] Must provide at least a conversion type, one input file name and an output file name.'
        print 'Usage:'
        print USAGE
        return


    conversion = args[1]
    output_filename = args[-1]
    input_filenames = args[2:-1]

    #Verify input.
    if os.path.exists(output_filename):
        print '[WARNING] Output file "'+output_filename+'" exists.'

        if overwrite_output == None:
            answer = queryYesNo('Do you want to overwrite?')
        else:
            answer = overwrite_output

        if answer:
            print 'Overwriting.'
        else:
            print 'Exiting.'
            return

    for input_filename in input_filenames:
        if not os.path.exists(input_filename):
            print '[ERROR] Input file "'+input_filename+'" does not exist.'
            return

    #Parse conversion
    if conversion in RUNRESULT_CONVERSIONS.keys():

        #Perform conversion
        if 'TAEeo2' in conversion:
            print 'Reading in TAE Evaluation Output...'
            runresults = readTAEEvaluationOutput(input_filenames)
        elif 'SATzilla12v12' in conversion:
            print 'Reading in SATzilla12v1 data...'
            runresults = readSATzilla12v1RunResults(input_filenames)
        elif 'SATzilla12v22' in conversion:
            print 'Reading in SATzilla12v1 data...'
            runresults = readSATzilla12v2RunResults(input_filenames)
        elif 'ZFrt2' in conversion:
            print 'Reading in Zilla Format data...'
            runresults = readZillaFormatRunResults(input_filenames)
        elif 'manual2' in conversion:
            print 'Reading in manually processable data...'
            runresults = readManualRunResults(input_filenames)
        else:
            raise Exception('Unrecognized conversion "'+conversion+'" in RUNRESULT_CONVERSIONS "'+str(RUNRESULT_CONVERSIONS.keys())+'".')

        #Verify data.
        valid = verifyRunResults(runresults)
        if not valid:
            if skip_valid == None:
                answer = queryYesNo('Run results data not valid, do you want to continue?')
            else:
                answer = skip_valid
            if answer:
                print 'Continuing.'
            else:
                print 'Exiting'
                return
        else:
            print 'Run results data is valid.'



        #Merge in special cases
        if conversion == 'ZFrt2ZFrt':

            if add_vbs == None:
                answer = queryYesNo('Add VBS?')
            else:
                answer = True
            if answer:
                print 'Adding VBS to data ...'
                addVBS(runresults)


            if one_algorithm == None:
                answer = queryYesNo('Merge all run results as if done by a single algorithm?')
            else:
                answer = True
            if answer:
                if one_algorithm == None:
                    answer2=False
                    while not answer2:
                        merge_algorithm = raw_input('Provide new algorithm name:')
                        answer2 = queryYesNo('Merging to algorithm "%s"?' % merge_algorithm)
                else:
                    merge_algorithm = one_algorithm

                print 'Merging all the data as if done by a single algorithm %s ...' % merge_algorithm
                mergeAlgorithms(runresults,merge_algorithm)


        #Remove unsolved instances if needed.
        unsolved_instances = getUnsolvedInstances(runresults)
        if len(unsolved_instances)>0:
            if len(unsolved_instances)<10:
                print 'There are some unsolved instances : %s .' % str(unsolved_instances)
            else:
                print 'There are %d unsolved instances.' % len(unsolved_instances)
            if remove_unsolved_instances == None:
                answer = queryYesNo('Remove unsolved instances?')
            else:
                answer = remove_unsolved_instances
            if answer:
                for instance in unsolved_instances:
                    del runresults[instance]

        #Write data.
        if '2ZFrt' in conversion:
            print 'Converting data to Zilla Format Run Results.'
            print 'Writing out converted run result data to "'+output_filename+'"...'
            writeZillaFormatRunResult(runresults,output_filename)
        elif '2manual' in conversion:
            print 'Converting data to manually processable format.'
            print 'Writing out converted run result data to "'+output_filename+'"...'
            writeManualRunResult(runresults,output_filename)

    elif conversion in FEATURE_CONVERSIONS.keys():
        print conversion
        print conversion == 'TAEeo2ZFf'
        if not len(input_filenames) == 1:
            print '[ERROR] Can only convert a single input file to features.'
            return
        #Perform conversion
        if conversion == 'TAEeo2ZFf':
            print 'Converting TAE Evaluation Output to Zilla Feature Format.'

            print 'Reading in data...'
            runresults = readTAEEvaluationOutput(input_filenames)
        elif conversion == 'SATzilla12f2ZFf':
            print 'Converting SATzilla12 features to Zilla Feature Format.'

            print 'Reading in data...'
            runresults = readSATzilla12Features(input_filenames[0])
        else:
            raise Exception('Unrecognized conversion "'+conversion+'" in FEATURE_CONVERSIONS "'+str(FEATURE_CONVERSIONS.keys())+'".')

        #Verify data.
        print 'Verifying integrity of  data...'
        valid = verifyRunResults(runresults)
        if not valid:
            answer = queryYesNo('Run results data not valid, do you want to continue?')
            if answer:
                print 'Continuing.'
            else:
                print 'Exiting'
                return
        else:
            print 'Run results data is valid.'

        #Write data.
        print 'Writing out converted features data to "'+output_filename+'"...'
        writeZillaFormatFeatures(runresults,output_filename)

    else:
        print 'Unrecognized conversion type "'+conversion+'".'
        print 'Usage:'
        print USAGE
        return

    return



########################################################################################################3




def queryYesNo(question):
    answer = None
    while not (answer == 'y' or answer == 'n'):
        answer = raw_input(question+' [y/n]').lower()

        if answer == 'y':
            return True
        elif answer == 'n':
            return False
        else:
            print 'Unrecognized answer, please try again.'

def getUnsolvedInstances(runresults):
    """
    Return a list of instances that are not solved by any algorithm.

    runresults - a run results dictionary taking problem instance to a dictionary taking algorithm to
    a tuple (runtime,runresult,cutoff).
        runresults[problem instance][algorithm] -> (runtime,runresult,cutoff)
    """
    unsolved_instances = []
    instances = runresults.keys()
    for instance in instances:
        unsolved = True
        for algorithm in runresults[instance].keys():
            (result,runtime,quality,cutoff,seed,additional_rundata) = runresults[instance][algorithm]
            if result == 'SAT' or result == 'UNSAT':
                unsolved = False
                break
        if unsolved:
            unsolved_instances.append(instance)

    return unsolved_instances

def verifyRunResults(runresults):
    """
    Verify the integrity of a run results dictionary. Namely that:
        - results are coherent across instances,
        - TIMEOUT only occurs when runtime is higher than cutoff,

    Returns True if and only if the run results dictionary is valid.

    runresults - a run results dictionary taking problem instance to a dictionary taking algorithm to
    a tuple (runtime,runresult,cutoff).
        runresults[problem instance][algorithm] -> (runtime,runresult,cutoff)
    """

    valid = True

    instances = runresults.keys()

    for instance in sorted(instances):
        results = set()
        for algorithm in runresults[instance].keys():
            (result,runtime,quality,cutoff,seed,additional_rundata) = runresults[instance][algorithm]

            if result == "TIMEOUT" and float(runtime) < float(cutoff):
                print '[WARNING] Run result for '+algorithm+' on '+instance+' is TIMEOUT but runtime ('+runtime+') is less than cutoff ('+cutoff+').'
                valid = False

            if float(runtime) > float(cutoff) and (result == 'SAT' or result == 'UNSAT'):
                print '[WARNING] Run result for '+algorithm+' on '+instance+' is '+result+' but runtime ('+runtime+') is larger than cutoff ('+cutoff+').'
                valid = False

            results.add(result)

        if "SAT" in results and "UNSAT" in results:
            print '[ERROR] An instance %s is both SAT and UNSAT according to different solvers.' % instance
            valid = False

    return valid


def mergeAlgorithms(runresults,merge_algorithm):
    """
    Merge all the results in the provided results as it they were all execute by a single algorithm.

    runresults - a run results dictionary taking problem instance to a dictionary taking algorithm to
    a tuple (runtime,runresult,cutoff).
        runresults[problem instance][algorithm] -> (runtime,runresult,cutoff)
    merge_algorithm - the name of the new algorithm.
    """

    instances = runresults.keys()
    for instance in instances:
        if len(runresults[instance].keys())>1:
            raise Exception('There are more than a single algorithm result for instance %s - merge impossible.' % instance)
        algorithm = runresults[instance].keys()[0]
        runresults[instance][merge_algorithm] = runresults[instance].pop(algorithm)
    return

def addVBS(runresults):
    instances = runresults.keys()
    algorithms = []
    for instance in instances:
        algorithms.extend(runresults[instance].keys())
    algorithms = list(set(algorithms))
    for algorithm in algorithms:
        if 'vbs' in algorithm.lower():
            raise Exception('VBS is apparently already in run results (algorithm : "%s")' % algorithm)
    for instance in instances:
        vbs_result = None
        vbs_runtime = None
        cutoff = None
        seed = 0
        for algorithm in algorithms:
            (r,t,q,c,s,a) = runresults[instance][algorithm]
            t = float(t)
            c = float(c)
            if cutoff == None:
                cutoff = c
            elif c != cutoff:
                raise ValueError('Cannot add VBS to a set of runs with different cutoffs (%.2f and %.2f)' % (cutoff,c))

            if vbs_runtime == None or t < vbs_runtime:
                if vbs_result in ['SAT','UNSAT']:
                    if r in ['SAT','UNSAT']:
                        vbs_result = r
                        vbs_runtime = t
                else:
                    vbs_runtime = t
                    vbs_result = r

            runresults[instance]['VBS'] = (str(vbs_result),str(vbs_runtime),str(vbs_runtime),str(cutoff),str(seed),'')


if __name__ == "__main__":
    main()
