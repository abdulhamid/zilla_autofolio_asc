import DataConverter
from DataConverter import *

def readTAEEvaluationOutput(input_filenames):
    """
    Converts a list of files in TAEEvaluationOutput format to a run result dictionary.

    input_filenames - a list of TAEEvaluationOutput formatted files.
    """

    #Check input
    if len(input_filenames)==0:
        raise Exception("Must provide at least one input file.")

    runresults = dict()

    #Read in the data
    for input_filename in input_filenames:
        print 'Reading data from "'+input_filename+'"...'
        with open(input_filename,'rb') as csvfile:
            csvreader = csv.reader(csvfile,delimiter=',',quotechar='"')

            #Skip header line
            header = csvreader.next()

            for line in csvreader:
                algorithm = line[0]
                cutoff = line[2]
                seed = line[3]
                instance = line[4]
                result = line[5]
                runtime = line[6]
                additional_rundata = line[7]

                quality = DataConverter.ZF_INCOMPLETE_QUALITY_VALUE

                if instance not in runresults.keys():
                    runresults[instance] = dict()

                if algorithm in runresults[instance].keys():
                    raise Exception('Duplicate results for algorithm '+algorithm+' on instance '+instance+'.')
                else:
                    runresults[instance][algorithm] = (result,runtime,quality,cutoff,seed,additional_rundata)

    return runresults