import sys
import matplotlib.pyplot as plt
import numpy as np
import mypyutils
import datetime
import scipy.stats
from ZillaRunDataAnalyzer import *

#Parse arguments.

args = sys.argv

show_plot = True
if '--no-plots' in args:
    show_plot = False
    args.remove('--no-plots')

no_vbs = False
if '--no-vbs' in args:
    no_vbs = True
    args.remove('--no-vbs')


for arg in args:
    if arg[:2] == '--':
        raise Exception('Unrecognized option %s.' % arg)
        sys.exit()

filenames = args[1:]

#Read in and organize data.

print 'Reading in data...'
rundata = readZillaRunData(filenames)

algorithms = []
for instance in rundata.keys():
    algorithms.extend(rundata[instance].keys())
algorithms = sorted(list(set(algorithms)))

if len(algorithms)<5:
    print 'Algorithms: %s.' % ', '.join(algorithms)
else:
    print '%d algorithms' % len(algorithms)

instances = filter(lambda i : set(algorithms)<=set(rundata[i].keys()),rundata.keys())
if len(instances) < len(rundata.keys()):
    print '%d instances do not have results for all algorithms.' % (len(rundata.keys())-len(instances))
print '%d instances.' % len(instances)


if not no_vbs:
    print 'Adding VBS...'
    addVBS(rundata,'VBS')
    algorithms.insert(0,'VBS')

palette = [plt.cm.gist_rainbow(1.0*i/float(len(algorithms))) for i in range(len(algorithms))]

print 'Organizing results...'

instances = sorted(instances, key=lambda i : sum([min(float(r),float(c)) if s=='SAT' or s=='UNSAT' else float(c) for (s,r,q,c,e) in [rundata[i][algorithm] for algorithm in algorithms]]))

runtimes = [0] * len(instances)
for i in range(len(instances)):
    instance = instances[i]
    runtimes[i] = [0]*len(algorithms)
    for j in range(len(algorithms)):
        algorithm = algorithms[j]
        (result,runtime,quality,cutoff,seed) = rundata[instance][algorithm]
        if result == 'SAT' or result == 'UNSAT':
            runtimes[i][j] = min(float(cutoff),float(runtime))
        else:
            runtimes[i][j] = float(cutoff)



#Summarize performance.

print 'Summarizing performances...'
print algorithms
num_solved = getNumberOfSolved(rundata,algorithms,instances)
median_runtimes = getMedianRuntimes(rundata,algorithms,instances)
mean_park = getMeanPARk(rundata,algorithms,instances,10)

num_solved = {algorithms[i] : num_solved[i] for i in range(len(algorithms))}
median_runtimes = {algorithms[i] : median_runtimes[i] for i in range(len(algorithms))}
mean_park = {algorithms[i] : mean_park[i] for i in range(len(algorithms))}

print '%-40s %25s %25s %25s %20s' % ('Algorithm', 'Runtime PAR10', 'Solved Instances (num)', 'Solved Instances (%)', 'Median Runtime')
print '-'*160
for algorithm in sorted(algorithms,key=lambda a : mean_park[a]):
    print '%-40s %25.2f %25d %25.1f%% %20.1f s' % (algorithm,mean_park[algorithm],num_solved[algorithm],float(num_solved[algorithm])/float(len(instances))*100,median_runtimes[algorithm])
print '-'*160



if show_plot:
    print 'Plotting heatmap...'

    fig = plt.figure(figsize=(10,10))
    mypyutils.pyplot.plotHeatMap(runtimes,['']*len(instances),algorithms,colormap=plt.cm.hot_r,minvalue=0)
    ticks=np.array([0,0.05,0.1,0.15,0.2,0.25,0.5,0.75,1])
    cbar = plt.colorbar(ticks=ticks*cutoff,orientation='vertical')
    cbar.set_ticklabels([str(datetime.timedelta(seconds=t*cutoff)) for t in ticks])
    cbar.ax.set_aspect(75)

    print 'Plotting ECDF...'
    plt.figure(figsize=(10,10))
    for i in range(len(algorithms)):
        runtimesi = [runtimes[k][i] for k in range(len(instances))]
        mypyutils.pyplot.plotECDF(runtimesi,color=palette[i],label=algorithms[i])

    plt.xlabel('runtime (s)')
    plt.xscale('log')
    yd=5
    yn=100/yd
    plt.yticks([float(i)/float(yn) for i in range(yn+1)])
    plt.ylabel('fraction of instances')
    plt.grid()
    plt.legend(loc="upper left",numpoints = 1)


    print 'Plotting correlation...'
    plt.figure(figsize=(10,10))
    n = len(algorithms)*len(algorithms)
    def plotfunc(i,x,y,gridw,gridh,ax):
        if x<y:
           ax.get_xaxis().set_visible(False)
           ax.get_yaxis().set_visible(False)
           ax.patch.set_visible(False)
           ax.axis('off')
        elif x == y:
            runtimesx = np.array([runtimes[k][x] for k in range(len(instances))])
            mypyutils.pyplot.plotECDF(runtimesx,label=algorithms[x],color=palette[x])
            plt.xscale('log')
            plt.xlabel('runtime (s)')
            plt.title(algorithms[x])
        else:
            runtimesx = np.array([runtimes[k][x] for k in range(len(instances))])
            runtimesy = np.array([runtimes[k][y] for k in range(len(instances))])


            (corr,pvalue) = scipy.stats.pearsonr(runtimesx,runtimesy)

            plt.scatter(runtimesx,runtimesy,alpha=0.3,color='k',marker='o',linewidth=0)

            plt.xscale('log')
            plt.yscale('log')
            plt.title('pearsonr = %.2f' % corr)
            plt.xlabel(algorithms[x])
            plt.ylabel(algorithms[y])

    if len(algorithms)>1:
        if len(algorithms)<5:
            mypyutils.pyplot.plotsGrid(n,plotfunc,sharex=True,sharey=True,diagonalnoshare=True)
            plt.tight_layout()
        else:
            corrs = [
                        [
                        scipy.stats.pearsonr(
                            np.array([runtimes[k][i] for k in range(len(instances))]),
                            np.array([runtimes[k][j] for k in range(len(instances))])
                            )[0]
                        for j in range(len(algorithms))
                        ]
                        for i in range(len(algorithms))
                    ]
            mypyutils.pyplot.plotHeatMap(corrs,algorithms,algorithms,colormap=plt.cm.seismic,minvalue=-1,maxvalue=1)
            cbar = plt.colorbar()
            cbar.ax.set_aspect(75)
            plt.title('Pearson Correlations')
    plt.show()



